import { inject } from 'aurelia-framework';
import { json } from 'aurelia-fetch-client';
import { BrainyHttpHandler } from './brainy-http-handler';

@inject(BrainyHttpHandler)
export class Client {

  constructor(http) {
    this.http = http;
  }

  getFrom(uri) {
    return this.http.fetch(uri);
  }

  postEmptyBodyTo(uri) {
    return this.http
        .fetch(uri, {
          method: 'post',
        });
  }

  postTo(uri, element) {
    return this.http
        .fetch(uri, {
          method: 'post',
          body: json(element),
        });
  }

  deleteFrom(uri) {
    return this.http
        .fetch(uri, {
          method: 'delete',
        });
  }

}
