import { inject } from 'aurelia-framework';
import { User } from '../models/user';
import { Skill } from '../models/skill';
import { Client } from './client';

@inject(Client)
export class ProfileClient {

  constructor(client) {
    this.client = client;
  }

  getProfileInformation(username) {
    return this.requestRawProfileInformation(username)
      .then(response => response.json())
      .then(response => new User(response));
  }

  requestRawProfileInformation(username) {
    return this.client.getFrom(`/profileApi/profile/${username}`);
  }

  saveUserInformation(user) {
    return this.client.postTo('/profileApi/profile', user)
      .then(response => response.json())
      .then(response => new User(response));
  }

  retrieveListOfProfiles(query) {
    return this.client.getFrom(`/profileApi/profiles?find=${query}`)
      .then(response => response.json())
      .then(response => response.elements.map(u => new User(u)));
  }

  retrieveMostEndorsedSkills(email) {
    return this.client.getFrom(`/profileApi/profile/${email}/mostEndorsedSkills`)
      .then(response => response.json())
      .then(response => response.elements.map(s => new Skill(s)));
  }
}
