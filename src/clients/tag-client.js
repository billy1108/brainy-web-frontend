import { inject } from 'aurelia-framework';
import { Tag } from '../models/tag';
import { Client } from './client';

@inject(Client)
export class TagClient {

  constructor(client) {
    this.client = client;
  }

  createTag(tag) {
    return this.client
        .postTo('/profileApi/my-tags', tag)
        .then(response => response.json())
        .then(jsonResponse => new Tag(jsonResponse));
  }

  deleteTag(idTag) {
    return this.client
        .deleteFrom(`/profileApi/my-tags/${idTag}`);
  }
}
