import { bindable, inject, NewInstance } from 'aurelia-framework';
import { UserDetailService } from '../services/user-detail-service';

@inject(NewInstance.of(UserDetailService))
export class UserDetail {

  @bindable() user;
  preloaderAllowed = false;
  progressValue = null;

  constructor(api) {
    this.api = api;
    this.api.setViewModel(this);
  }

  showSubDetail() {
    if (this.collapsed === undefined) {
      this.preloaderAllowed = true;
      this.collapsed = true;

      this.api
        .getMostEndorsedSkills(this.user.email)
        .then(() => {
          this.preloaderAllowed = false;
        });
    } else {
      this.collapsed = !this.collapsed;
    }
  }

  goToProfile() {
    this.api.navigateToProfile(this.user);
  }

  setSkills(skills) {
    this.user.skills = skills;
  }
}
