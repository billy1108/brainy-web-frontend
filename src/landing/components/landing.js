import { inject } from 'aurelia-framework';
import { BaseComponent } from '../../util/base-component';
import { ProfileService } from '../../profile/services/profile-service';
import { AuthHelper } from '../../util/auth-helper';
import { constants } from '../../util/constants';
import localStorageManager from '../../util/local-storage-manager';

@inject(ProfileService, AuthHelper)
export class Landing extends BaseComponent {

  constructor(profileService, authHelper) {
    super();
    this.profileService = profileService;
    this.authHelper = authHelper;
    this.profileService.setViewModel(this);
  }

  login() {
    this.authHelper.login();
  }

  activate(params) {
    this.params = this.authHelper.parseQueryString(params.token);
  }

  attached() {
    this.validateUrlParams();
    this.notify('enterLandingPage');
  }

  detached() {
    this.notify('leaveLandingPage');
  }

  validateUrlParams() {
    const accessToken = this.params[constants.ACCESS_TOKEN];

    if (this.authHelper.validateJWT(accessToken)) {
      localStorageManager.setAuthToken(accessToken);
      this.profileService.loadInitialProfileInformation()
        .then((response) => {
          if (response.status === constants.FIRST_LOGIN_STATUS) {
            localStorageManager.setFirstLogin(true);
            this.navigation.navigateToProfile();
          } else if (response.status === constants.SUCCESS_STATUS) {
            this.navigation.navigateToHome();
          } else {
            localStorageManager.removeAuthToken();
            this.navigation.navigateToLogin();
          }
        });
    } else if (accessToken) {
      this.showMessageError('Please use the corporate e-mail account.');
    } else if (this.auth.isAuthenticated()) {
      this.navigation.navigateToHome();
    }
  }

  setUser() {
    this.navigation.navigateToHome();
  }

}
