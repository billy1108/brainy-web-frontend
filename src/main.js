import { load } from 'aurelia-environment';
import 'materialize';

export function configure(aurelia) {
  load().then(() => {
    aurelia.use
    .standardConfiguration()
    .developmentLogging()
    .plugin('aurelia-materialize-bridge', bridge => bridge.useAll())
    .plugin('aurelia-auth')
    .plugin('aurelia-dialog')
    .feature('resources/custom-elements');

    aurelia.start().then(a => a.setRoot());
  });
}
