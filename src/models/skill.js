import { constants } from '../util/constants';

export class Skill {

  constructor(attributes = {}) {
    this.id = attributes.id;
    this.name = attributes.name || '';
    this.supports = attributes.supports || 0;
    this.validated = attributes.validated;
    this.myLevel = attributes.myLevel || 0;
    this.averageLevel = attributes.averageLevel || 0;
    this.guestLevel = attributes.guestLevel || 0;
  }

  get isComplete() {
    const nameAtLeastHasTwoLetters = this.name && this.name.length > 1;
    return !!(nameAtLeastHasTwoLetters && this.myLevel);
  }

  get isValidated() {
    return this.isComplete && constants.SKILL_REGEX.test(this.name);
  }
}
