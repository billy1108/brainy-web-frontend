import { constants } from '../util/constants';

export class Tag {

  constructor(attributes = {}) {
    this.id = attributes.id;
    this.name = attributes.name || '';
  }

  get isValid() {
    return constants.TAG_REGEX.test(this.name) && this.name.length <= 30;
  }

  isDuplicatedIn(tags) {
    return tags.some(tag => tag.name === this.name);
  }

}
