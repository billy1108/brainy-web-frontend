import { bindable } from 'aurelia-framework';
import { BaseComponent } from '../../util/base-component';
import { constants } from '../../util/constants';
import localStorageManager from '../../util/local-storage-manager';

export class NavBar extends BaseComponent {

  @bindable router = null;
  isAuthenticated = false;
  isFirstLogin = false;
  notificationAllowed = constants.NOTIFICATION_ALLOWED;

  constructor() {
    super();
    this.isAuthenticated = this.auth.isAuthenticated();
  }

  attached() {
    this.subscriber = this.ea.subscribe(constants.UPDATE_NAV_BAR_WHEN_USER_LOGGED, () => {
      this.updateNavBarWhenUserLogged();
    });
  }

  updateNavBarWhenUserLogged() {
    this.isAuthenticated = true;
    this.isFirstLogin = localStorageManager.isFirstLogin();
  }

  detached() {
    this.subscriber.dispose();
  }

  logout() {
    this.isAuthenticated = false;
    this.isFirstLogin = false;
    localStorageManager.removeAuthToken();
    localStorageManager.setFirstLogin(this.isFirstLogin);
    this.navigation.navigateToLogin();
  }
}
