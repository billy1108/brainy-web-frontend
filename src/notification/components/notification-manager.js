import { bindable, inject } from 'aurelia-framework';
import { EventAggregator } from 'aurelia-event-aggregator';
import { NotificationService } from '../services/notification-service';

@inject(NotificationService, EventAggregator)
export class NotificationManager {

  @bindable() notifications = [];
  hasNotifications = false;

  constructor(notificationService, eventAggregator) {
    this.notificationService = notificationService;
    this.eventAggregator = eventAggregator;
    this.notificationService.setViewModel(this);
  }

  attached() {
    this.notificationService.retrieveNotifications();
  }

  setNotifications(notifications) {
    const total = notifications.length;
    this.notifications = notifications;
    this.hasNotifications = total > 0;
  }
}
