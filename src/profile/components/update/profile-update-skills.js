import { bindable, inject } from 'aurelia-framework';
import { ProfileService } from '../../services/profile-service';
import { Skill } from '../../../models/skill';
import constantsMessage from '../../../util/constants-message';

@inject(ProfileService)
export class ProfileUpdateSkills {

  @bindable() user;

  constructor(profileService) {
    this.profileService = profileService;
    this.skill = new Skill();
  }

  addSkill() {
    this.skill.name = this.skill.name.toLowerCase();

    if (!this.skill.isValidated) {
      this.profileService.showMessageError(constantsMessage.SKILL_INVALID_MESSAGE);
    } else if (this.user.skillList.length >= 30) {
      this.profileService.showMessageError(constantsMessage.SKILL_LIMITED_MESSAGE);
    } else if (!this.isCurrentSkillListHasNewOne()) {
      this.profileService.showMessageError(constantsMessage.SKILL_ALREADY_EXIST_MESSAGE);
    } else {
      this.user.skillList.push(this.skill);
      this.skill = new Skill();
    }
  }

  isCurrentSkillListHasNewOne() {
    return !this.user.skillList.find(x => this.skill.name.toLowerCase() === x.name.toLowerCase());
  }

  removeFromSkillList(skill) {
    this.user.skillList = this.user.skillList.filter(s => s !== skill);
  }
}
