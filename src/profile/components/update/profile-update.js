import { inject } from 'aurelia-framework';
import { ProfileService } from '../../services/profile-service';
import { BadgeService } from '../../services/badge-service';
import { Navigation } from '../../../util/navigation';
import localStorageManager from '../../../util/local-storage-manager';

@inject(ProfileService, BadgeService, Navigation)
export class ProfileUpdate {

  isFirstLogin = false;

  constructor(profileService, badgeService, navigation) {
    this.navigation = navigation;
    this.profileService = profileService;
    this.badgeService = badgeService;
    this.profileService.setViewModel(this);
    this.badgeService.setViewModel(this);
  }

  setUser(value) {
    this.user = value;
  }

  getBadgesLength() {
    return this.badges.length;
  }

  setBadges(badges) {
    this.badges = badges;
  }

  attached() {
    this.isFirstLogin = localStorageManager.isFirstLogin();
    this.getUserInformation();
    this.profileService.notifyUpdateNavBarWhenUserLogged();
  }

  getUserInformation() {
    this.user = this.profileService.getBasicUserInformation();
    if (!this.isFirstLogin) {
      this.getUserInformationWithUserName(this.user.email);
    }
  }

  getUserInformationWithUserName(username) {
    this.profileService
      .retrieveProfileInformation(username)
      .then(() => {
        this.badgeService.retrieveBadges(username);
      });
  }

  save() {
    this.profileService.saveUserInformation(this.user)
      .then(() => {
        this.badgeService.checkNewBadges(this.user.email);
      });
  }

  backHome() {
    this.navigation.navigateToHome();
  }

  navigateToProfileOfMe() {
    this.navigation.navigateToProfileOf(this.user.email);
  }

}
