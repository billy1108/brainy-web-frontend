import { bindable, inject } from 'aurelia-framework';
import { EventAggregator } from 'aurelia-event-aggregator';
import { ProfileService } from '../../services/profile-service';
import { BadgeService } from '../../services/badge-service';
import { TagService } from '../../../tag/services/tag-service';
import { UserTagService } from '../../../tag/services/user-tag-service';
import { BaseComponent } from '../../../util/base-component';
import constantsMessage from '../../../util/constants-message';

@inject(ProfileService, BadgeService, TagService, UserTagService, BaseComponent, EventAggregator)
export class ProfileView {

  @bindable() tags = [];
  @bindable() usertags = [];
  @bindable() badges = [];

  // TODO split the responsibilities in this file
  constructor(profileService, badgeService, tagService,
    userTagService, baseComponent, eventAggregator) {
    this.profileService = profileService;
    this.badgeService = badgeService;
    this.tagService = tagService;
    this.userTagService = userTagService;
    this.baseComponent = baseComponent;
    this.eventAggregator = eventAggregator;
    this.profileService.setViewModel(this);
    this.badgeService.setViewModel(this);
    this.tagService.setViewModel(this);
    this.userTagService.setViewModel(this);
  }

  bind() {
    this.subscriptionForTagCreated = this.eventAggregator
        .subscribe('tag-to-create', tag => this.createTag(tag));
  }

  unbind() {
    this.subscriptionForTagCreated.dispose();
  }

  activate(paramsFromUrl) {
    this.username = paramsFromUrl.username;
  }

  attached() {
    this.profileService
      .retrieveProfileInformation(this.username)
      .then(() => {
        this.badgeService.retrieveBadges(this.username);
        this.userTagService.getTagListByUser();
        this.userTagService.getMyTagsOnUser(this.username);
      });
  }

  setBadges(badges) {
    this.badges = badges;
  }

  setUser(user) {
    this.user = user;
  }

  setTags(tags) {
    this.tags = tags;
  }

  setTagsOnUser(tagsOnUser) {
    this.usertags = tagsOnUser;
  }

  tagUser(selectedTag) {
    this.userTagService.tagUser(this.username, selectedTag);
  }

  createTag(tag) {
    this.tagService.createTag(tag);
  }

  notifyTagCreated() {
    this.eventAggregator.publish('tag-created');
  }

  isQuantityOfTagsAllowed() {
    return this.tags.length <= 29;
  }

  openNewTagModal() {
    if (this.isQuantityOfTagsAllowed()) {
      this.eventAggregator.publish('edit-tag-to-insert');
    } else {
      this.tagService
          .showErrorMessage(constantsMessage.TAG_LIMITED_MESSAGE);
    }
  }

  deleteTagOnUser(idTag) {
    this.userTagService.deleteTagOnUser(this.username, idTag);
  }

  back() {
    if (this.user.isCurrentUser()) {
      this.baseComponent.navigation.navigateToProfile();
    } else {
      this.baseComponent.navigation.navigateToHome();
    }
  }
}
