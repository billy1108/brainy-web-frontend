import { bindable, inject, LogManager } from 'aurelia-framework';
import { SkillBarService } from '../../services/skill-bar-service';
import { BaseComponent } from '../../../util/base-component';
import { constants } from '../../../util/constants';

@inject(SkillBarService)
export class SkillRating extends BaseComponent {
  static DISABLED_STYLE = 'disabled';

  @bindable() user;

  constructor(api) {
    super();
    this.api = api;
    this.logger = LogManager.getLogger(SkillRating.name);
  }

  endorse(skill) {
    if (!this.user.isCurrentUser()) {
      this.processUpdateEndorse(skill);
    }
  }

  updateRateEndorse() {
    return (skill) => {
      this.endorse(skill);
    };
  }

  deleteRateEndorse(skill) {
    if (!this.user.isCurrentUser()) {
      this.processDeleteEndorse(skill);
    }
  }

  processUpdateEndorse(skill) {
    /* eslint-disable no-param-reassign */
    skill.preloaderAllowed = true;

    this.api.makeSkillEndorsement(this.user.email, skill)
      .then((response) => {
        skill.averageLevel = response.averageLevel;
        skill.validated = true;
      })
      .catch((error) => {
        this.logger.error('Details of the error:', error);
        this.showMessageError();
      })
      .then(() => {
        skill.preloaderAllowed = false;
      });
    /* eslint-enable no-param-reassign */
  }

  processDeleteEndorse(skill) {
    /* eslint-disable no-param-reassign */
    skill.preloaderAllowed = true;

    this.api.deleteSkillEndorsement(this.user.email, skill)
      .then((response) => {
        skill.averageLevel = response.value;
        skill.guestLevel = 0;
        skill.validated = false;
      })
      .catch((error) => {
        this.logger.error('Details of the error:', error);
        this.showMessageError();
      })
      .then(() => {
        skill.preloaderAllowed = false;
      });
    /* eslint-enable no-param-reassign */
  }

  getStyleEndorse() {
    return this.user.isCurrentUser() ? SkillRating.DISABLED_STYLE :
                                       constants.EMPTY_STRING;
  }
}
