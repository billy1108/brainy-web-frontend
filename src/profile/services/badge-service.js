import { inject, LogManager } from 'aurelia-framework';
import { BaseComponent } from '../../util/base-component';
import { BadgeClient } from '../../clients/badge-client';

@inject(BaseComponent, BadgeClient)
export class BadgeService {

  constructor(baseComponent, badgeClient) {
    this.baseComponent = baseComponent;
    this.badgeClient = badgeClient;
    this.logger = LogManager.getLogger(BadgeService.name);
  }

  setViewModel(value) {
    this.viewModel = value;
  }

  retrieveBadges(username) {
    this.baseComponent.showProgressHub();

    return this.badgeClient.getBadges(username)
      .then((response) => {
        this.viewModel.setBadges(response);
        return response;
      })
      .catch((error) => {
        this.logger.error('Details of the error:', error);
        this.baseComponent.showMessageError();
        return error;
      })
      .then((response) => {
        this.baseComponent.dismissProgressHub();
        return response;
      });
  }

  checkNewBadges(username) {
    return this.retrieveBadges(username)
      .then((response) => {
        if (response.stack) {
          return;
        }

        const badgesLength = this.viewModel.getBadgesLength();
        const newBadges = response.length - badgesLength;

        if (newBadges > 0) {
          const newBadgesMessage = `Awesome! You have ${newBadges} new badges!`;
          this.baseComponent.showMessageSuccess(newBadgesMessage);
        }
      });
  }
}
