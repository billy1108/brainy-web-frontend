/* global jwt_decode */

import { inject, LogManager } from 'aurelia-framework';
import { BaseComponent } from '../../util/base-component';
import { User } from '../../models/user';
import { constants } from '../../util/constants';
import { ProfileClient } from '../../clients/profile-client';
import localStorageManager from '../../util/local-storage-manager';

@inject(BaseComponent, ProfileClient)
export class ProfileService {

  constructor(baseComponent, profileClient) {
    this.baseComponent = baseComponent;
    this.profileClient = profileClient;
    this.logger = LogManager.getLogger(ProfileService.name);
  }

  setViewModel(value) {
    this.viewModel = value;
  }

  getBasicUserInformation() {
    this.baseComponent.notify(constants.UPDATE_NAV_BAR_WHEN_USER_LOGGED);
    return this.obtainUserFromJwt();
  }

  retrieveProfileInformation(username) {
    this.baseComponent.showProgressHub();

    return this.profileClient.getProfileInformation(username)
      .then((user) => {
        const isCurrentUser = localStorageManager.isCurrentUser(user.email);
        user.setCurrentUser(isCurrentUser);
        this.viewModel.setUser(user);
      })
      .catch((error) => {
        this.logger.error('Details of the error', error);
        this.baseComponent.showMessageError();
      })
      .then(() => {
        this.baseComponent.dismissProgressHub();
      });
  }

  loadInitialProfileInformation() {
    const user = this.obtainUserFromJwt();
    return this.profileClient.requestRawProfileInformation(user.email);
  }

  saveUserInformation(user) {
    this.baseComponent.showProgressHub();

    return this.profileClient.saveUserInformation(user)
      .then((response) => {
        this.updateWidgetWhenUserLogged();
        this.baseComponent.showMessageSuccess('Your profile was successfully updated!');
        this.viewModel.setUser(response);
      })
      .catch((error) => {
        this.logger.error('Details of the error:', error);
        this.baseComponent.showMessageError();
      })
      .then(() => {
        this.baseComponent.dismissProgressHub();
      });
  }

  updateWidgetWhenUserLogged() {
    localStorageManager.setFirstLogin(false);
    this.notifyUpdateNavBarWhenUserLogged();
  }

  notifyUpdateNavBarWhenUserLogged() {
    this.baseComponent.notify(constants.UPDATE_NAV_BAR_WHEN_USER_LOGGED);
  }

  showMessageError(message) {
    this.baseComponent.showMessageError(message);
  }

  obtainUserFromJwt() {
    const jsonBody = localStorageManager.getJsonWebTokenBody();

    return new User({
      names: jsonBody.name,
      email: jsonBody.unique_name,
    });
  }

}
