export function configure(config) {
  config.globalResources([
    './brainy-autocomplete-drop-down-list',
    './brainy-input',
    './create-tag-modal',
    './rating',
    './brainy-rating',
  ]);
}
