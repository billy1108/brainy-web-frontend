import { bindable } from 'aurelia-framework';

export class Rating {

  @bindable() rating = {};
  @bindable() settings = {};
  @bindable() updateRate;

  decimalLevel = 0.0;
  maxRating = 5;

  attached() {
    this.ratingPropertyName = this.settings.ratingPropertyName;
    this.starPropertyName = this.settings.starPropertyName;
  }

  rate(event) {
    if (event.target.dataset.rate && !this.settings.isView) {
      const rate = event.target.dataset.rate;
      this.rating[this.ratingPropertyName] = rate;
      this.updateRateInEndorsement();
    }
  }

  updateRateInEndorsement() {
    if (this.updateRate != null) {
      this.updateRate(this.rating);
    }
  }

  typeOfRatingClassName() {
    return this.settings.isView ? 'rate-viewer' : 'rate';
  }

  // TODO refactor to decrease the complexity of the method
  typeOfRatingStar(index) {
    const propertyRating = (this.settings.starPropertyName) ? this.settings.starPropertyName :
                                                            this.settings.ratingPropertyName;

    const ratingStarPropertyName = this.rating[this.settings.starPropertyName];
    const decimalDifference = ratingStarPropertyName - Math.floor(ratingStarPropertyName);
    let starClass;

    if (index > (this.rating[propertyRating] || 0)) {
      if ((index === Math.round(ratingStarPropertyName)) && (decimalDifference !== 0)) {
        starClass = this.setStarStyleByDecimalDiff(decimalDifference);
      } else {
        starClass = 'star_border';
      }
    } else {
      starClass = 'star';
    }

    if (!this.settings.isView) {
      starClass = 'star';
    }

    return starClass;
  }

  setStarStyleByDecimalDiff(decimalDifference) {
    let starType = 'star_border';

    if ((decimalDifference >= 0.3) && (decimalDifference <= 0.7)) {
      starType = 'star_half';
    } else if (decimalDifference > 0.7) {
      starType = 'star';
    }

    return starType;
  }

}
