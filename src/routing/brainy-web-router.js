import { inject } from 'aurelia-framework';
import { AuthorizeStep } from 'aurelia-auth';
import { Router } from 'aurelia-router';
import routes from './routes';
import { AuthorizeUserRegisteredStep } from '../util/authorize-user-registered-step';

@inject(Router)
export class BrainyWebRouter {

  constructor(router) {
    this.router = router;
  }

  configure() {
    /* eslint no-param-reassign: ["error", { "props": false }] */
    const appRouterConfig = (config) => {
      config.title = 'Brainy';
      config.addAuthorizeStep(AuthorizeStep);
      config.addAuthorizeStep(AuthorizeUserRegisteredStep);
      config.map(routes);
    };

    this.router.configure(appRouterConfig);
  }
}
