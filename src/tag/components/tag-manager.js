import { bindable, inject } from 'aurelia-framework';
import { EventAggregator } from 'aurelia-event-aggregator';
import { TagService } from '../services/tag-service';
import { UserTagService } from '../services/user-tag-service';
import constantsMessage from '../../util/constants-message';

@inject(TagService, UserTagService, EventAggregator)
export class TagManager {

  @bindable() tags = [];
  @bindable() users = [];
  hasResults = false;
  tagSelected = null;

  constructor(tagService, userTagService, eventAggregator) {
    this.tagService = tagService;
    this.userTagService = userTagService;
    this.eventAggregator = eventAggregator;
    this.tagService.setViewModel(this);
    this.userTagService.setViewModel(this);
  }

  bind() {
    this.subscriptionForTagCreated = this.eventAggregator
        .subscribe('tag-to-create', tag => this.createTag(tag));
  }

  unbind() {
    this.subscriptionForTagCreated.dispose();
  }

  attached() {
    this.userTagService.getTagListByUser();
  }

  /*
  *  Dereference tags after the component is removed from DOM.
  */
  detached() {
    this.tags = [];
  }

  setTags(values) {
    this.tags = values;
  }

  setUsers(usersFound) {
    this.users = usersFound;
  }

  isQuantityOfTagsAllowed() {
    return this.tags.length <= 29;
  }

  createTag(tag) {
    this.tagService.createTag(tag);
  }

  openNewTagModal() {
    if (this.isQuantityOfTagsAllowed()) {
      this.eventAggregator.publish('edit-tag-to-insert');
    } else {
      this.tagService
          .showErrorMessage(constantsMessage.TAG_LIMITED_MESSAGE);
    }
  }

  notifyTagCreated() {
    this.eventAggregator.publish('tag-created');
    this.userTagService.getTagListByUser();
  }

  findUsersByTag(tag, index) {
    this.hasResults = true;
    this.tagSelected = tag;
    this.tagSelected.index = index;
    this.userTagService.findUsersByTagName(tag.name);
  }

  deleteTag() {
    this.tagService.deleteTag(this.tagSelected.id);
  }

  removeTagSelected() {
    this.tags.splice(this.tagSelected.index, 1);
    this.tagSelected = null;
  }

}
