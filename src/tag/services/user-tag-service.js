import { inject, LogManager } from 'aurelia-framework';
import { BaseComponent } from '../../util/base-component';
import { constants } from '../../util/constants';
import { UserTagClient } from '../../clients/user-tag-client';

@inject(BaseComponent, UserTagClient)
export class UserTagService {

  constructor(baseComponent, userTagClient) {
    this.baseComponent = baseComponent;
    this.userTagClient = userTagClient;
    this.logger = LogManager.getLogger(UserTagService.name);
  }

  setViewModel(value) {
    this.viewModel = value;
  }

  getTagListByUser() {
    this.baseComponent.showProgressHub();

    return this.userTagClient
        .getTagListByUser()
        .then((tags) => {
          this.viewModel.setTags(tags);
        })
        .catch((error) => {
          this.logger.error('Details of the error:', error);
          this.viewModel.setTags([]);
          this.baseComponent.showMessageError();
        })
        .then(() => {
          this.baseComponent.dismissProgressHub();
        });
  }

  getMyTagsOnUser(email) {
    return this.userTagClient
        .getMyTagsOnUser(email)
        .then((tags) => {
          this.viewModel.setTagsOnUser(tags);
        });
  }

  tagUser(email, idTag) {
    return this.userTagClient
        .tagUser(email, idTag)
        .then((response) => {
          if (response.status === constants.SUCCESS_STATUS) {
            this.baseComponent.showMessageSuccess('Your buddy was tagged');
            this.getMyTagsOnUser(email);
          } else {
            this.logger.error('Response:', response);
            this.baseComponent.showMessageError('Cannot tag yourself');
          }
        })
        .catch((error) => {
          this.logger.error('Details of the error:', error);
          this.baseComponent.showMessageError();
        })
        .then(() => {
          this.baseComponent.dismissProgressHub();
        });
  }

  deleteTagOnUser(email, idTag) {
    this.baseComponent.showProgressHub();

    return this.userTagClient
      .deleteTagOnUser(email, idTag)
      .then(() => {
        this.baseComponent.showMessageSuccess('The tag was removed from the pal');
      })
      .catch((error) => {
        this.logger.error('Details of the error:', error);
        this.baseComponent.showMessageError();
      })
      .then(() => {
        this.baseComponent.dismissProgressHub();
      });
  }

  findUsersByTagName(tagName) {
    this.baseComponent.showProgressHub();

    this.userTagClient.findUsersByTagName(tagName)
      .then((users) => {
        this.viewModel.setUsers(users);
      })
      .catch((error) => {
        this.logger.error('Details of the error:', error);
        this.viewModel.setUsers([]);
        this.baseComponent.showMessageError();
      })
      .then(() => {
        this.baseComponent.dismissProgressHub();
      });
  }
}
