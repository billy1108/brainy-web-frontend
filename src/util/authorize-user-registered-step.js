import { inject } from 'aurelia-framework';
import { Redirect } from 'aurelia-router';
import { AuthService } from 'aurelia-auth';
import localStorageManager from './local-storage-manager';

@inject(AuthService)
export class AuthorizeUserRegisteredStep {

  principalFirstLoginRoute = 'myprofile';
  isLoggedIn;
  isFirstLogin;

  constructor(auth) {
    this.auth = auth;
  }

  run(routingContext, next) {
    this.updateAttributes();

    if (this.isLoggedInAndAccessToPageNotAllowedToUserFirstLogin(routingContext)) {
      return next.cancel(new Redirect(this.principalFirstLoginRoute));
    }

    return next();
  }

  isLoggedInAndAccessToPageNotAllowedToUserFirstLogin(routingContext) {
    return this.isLoggedIn
        && this.isFirstLogin
        && routingContext.getAllInstructions().some(i => !i.config.authFirstLogin);
  }

  updateAttributes() {
    this.isLoggedIn = this.auth.isAuthenticated();
    this.isFirstLogin = localStorageManager.isFirstLogin();
  }
}
