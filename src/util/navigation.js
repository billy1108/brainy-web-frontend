import { inject } from 'aurelia-framework';
import { Router } from 'aurelia-router';

@inject(Router)
export class Navigation {

  constructor(router) {
    this.router = router;
  }

  navigateToHome() {
    this.router.navigate('home');
  }

  navigateToLogin() {
    this.router.navigate('login');
  }

  navigateToProfile() {
    this.router.navigate('myprofile');
  }

  navigateToProfileOf(email) {
    this.router.navigate(`profile/${email}`);
  }

  navigateToTagManager() {
    this.router.navigate('mytags');
  }

}
