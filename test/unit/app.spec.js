import {App} from '../../src/app';
import {Router} from 'aurelia-router';
import {EventAggregator} from 'aurelia-event-aggregator';

describe('the app configuration', () => {  
  let appBrainy;
  
  beforeEach(() => {
    let router = new Router();
    let eventAggregator = new EventAggregator();
    appBrainy = new App(router, eventAggregator);
  });

  it('Default "spinnerAllowed" property is "false" ', () => {
    expect(appBrainy.spinnerAllowed).toEqual(false);
  });

  it(' "spinnerAllowed" must be "true", when "showProgressHub" is called ', () => {
    appBrainy.showProgressHub();
    expect(appBrainy.spinnerAllowed).toEqual(true);
  });

  it(' "spinnerAllowed" must be "false", when "dismissProgressHub" is called ', () => {
    appBrainy.dismissProgressHub();
    expect(appBrainy.spinnerAllowed).toEqual(false);
  });

});
