import { BrainyWebRouter } from '../../src/routing/brainy-web-router';
import { Router } from 'aurelia-router';

xdescribe('WebRouter', () => {
  let brainyWebRouter;
  let router;

  beforeEach(() => {
    router = new Router();
    brainyWebRouter = new BrainyWebRouter(router);
    brainyWebRouter.configure();
  });

  it('should configure the router', () => {
    expect(router.isConfigured).toBe(true);
  });
});
