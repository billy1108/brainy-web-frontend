import {Skill} from '../../../src/models/skill.js';

describe('Skill', () => {

  let someSkill;

  beforeEach(() => {
    someSkill = new Skill();
  });

  describe('isComplete', () => {
    it('should return true if the skill name has two letters at least and my level', () => {
      someSkill = new Skill({name: 'ruby', myLevel: 4});
      expect(someSkill.isComplete).toBe(true);
    });

    it('should return false if the skill name has only a letter', () => {
      someSkill = new Skill({name: 'c', myLevel: 2});
      expect(someSkill.isComplete).toBe(false);
    });

    it('should return false if my level is not set', () => {
      someSkill = new Skill({name: 'java'});
      expect(someSkill.isComplete).toBe(false);
    });

    it('should return false if skill is created with default values', () => {
      expect(someSkill.isComplete).toBe(false);
    });
  });

  describe('isValidated', () => {
    it('should return true if it is complete and passes regex', () => {
      someSkill = new Skill({name: 'python', myLevel: 2});
      expect(someSkill.isValidated).toBe(true);
    });

    it('should return false if it is not complete', () => {
      someSkill = new Skill({name: 'python'});
      expect(someSkill.isValidated).toBe(false);
    });

    it('should return false if it does not pass regex', () => {
      someSkill = new Skill({name: 'super_skill'});
      expect(someSkill.isValidated).toBe(false);
    });
  });
});
