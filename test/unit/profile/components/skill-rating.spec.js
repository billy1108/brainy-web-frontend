import { StageComponent } from 'aurelia-testing';
import { bootstrap } from 'aurelia-bootstrapper';

xdescribe('SkillRating', () => {

  let skillRatingComponent;
  let theUser;

  beforeEach(() => {
    theUser = {
      skillList: [
        { name: 'ruby', averageLevel: 3, guestLevel: 1, myLevel: 4 },
        { name: 'java', averageLevel: 2, guestLevel: 2, myLevel: 2 },
        { name: 'golang', averageLevel: 5, guestLevel: 2, myLevel: 1 },
      ]
    };

    skillRatingComponent = StageComponent
      .withResources('profile/components/view/skill-rating')
      .inView('<skill-rating user.bind="user"></skill-rating>')
      .boundTo({ user: theUser });

    skillRatingComponent.bootstrap(aurelia => {
      aurelia.use.standardConfiguration()
        .feature('resources/custom-elements');
    });
  });

  afterEach(() => {
    skillRatingComponent.dispose();
  });

  it('should list the user skills after bound', (done) => {
    skillRatingComponent.create(bootstrap).then(() => {
      const skills = document.querySelectorAll('.user-skill .user-skill-name > div');
      expect(skills.length).toBe(3);
      done();
    }).catch((e) => { console.log(e.toString()) });
  });
});
