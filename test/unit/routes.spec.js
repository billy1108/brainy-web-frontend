import routes from '../../src/routing/routes';

describe('Routes', () => {

  it('should route to Landing when no url is defined', () => {
    const route = routes[0];
    expect(route.route).toContain('landing');
    expect(route.name).toBe('landing');
    expect(route.moduleId).toBe('./landing/components/landing');
    expect(route.nav).toBe(false);
    expect(route.title).toBe('Landing');
    expect(route.auth).toBe(false);
  });

  it('should route to Home when url is "home" ', () => {
    const route = routes[1];
    expect(route.route).toBe('home');
    expect(route.name).toBe('home');
    expect(route.moduleId).toBe('./home/components/home');
    expect(route.nav).toBe(true);
    expect(route.title).toBe('Home');
    expect(route.auth).toBe(true);
  });

  it('should route to My Profile when url is "myprofile" ', () => {
    const route = routes[2];
    expect(route.route).toBe('myprofile');
    expect(route.name).toBe('profileEdit');
    expect(route.moduleId).toBe('./profile/components/update/profile-update');
    expect(route.nav).toBe(true);
    expect(route.title).toBe('Profile Edition');
    expect(route.auth).toBe(true);
  });

  it('should route to View Profile when url is "profile" ', () => {
    const route = routes[3];
    expect(route.route).toBe('profile/:username');
    expect(route.name).toBe('profileDetail');
    expect(route.moduleId).toBe('./profile/components/view/profile-view');
    expect(route.nav).toBe(false);
    expect(route.title).toBe('Profile Detail');
    expect(route.auth).toBe(true);
  });
});
