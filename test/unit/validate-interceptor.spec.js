import {ValidateInterceptor} from '../../src/util/validate-interceptor';

describe('ValidateInterceptor', () => {
  let validateInterceptor;
  let authMock = { isAuthenticated: () => {} };
  let eaMock = { publish: () => {}};
  
  beforeEach(() => {
    spyOn(eaMock, 'publish').and.returnValue({});
    validateInterceptor = new ValidateInterceptor({}, eaMock, authMock);
  });

  describe('#currentUserAuthenticated', () => {

    it('should publish to event aggregator when user is authenticated', () => {
      spyOn(authMock, 'isAuthenticated').and.returnValue(true);
      validateInterceptor.currentUserAuthenticated();
      expect(eaMock.publish).toHaveBeenCalledWith('updateNavBarWhenUserLogged');
    });

    it('should not publish to event aggregator when user is unauthenticated', () => {
      spyOn(authMock, 'isAuthenticated').and.returnValue(false);
      validateInterceptor.currentUserAuthenticated();
      expect(eaMock.publish).not.toHaveBeenCalledWith('userLogged');
    });

  });
});
